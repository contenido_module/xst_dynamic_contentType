?><?php
/***********************************************
 * Modulname :   xst_dynamic_contentType
 * Author(s) :   Samuel Suther
 * Copyright :   Suther Kommunikationsdesign (www.suther.de)
 ************************************************/
?>

<table border="0">
	<tr>
		<td><?php echo mi18n("Element ID");?></td>
		<td><input type="text" name="<?php echo "CMS_VAR[1]";?>" value="<?php echo "CMS_VALUE[1]"; ?>"></td>
	</tr>
	<tr>
		<td><?php echo mi18n("Content Type");?>
			<div style="display:inline" title="Beispiel: CMS_DATE, CMS_HEAD, CMS_TEXT usw. Siehe auch: http://www.contenido-wiki.org/wiki/index.php?title=CMS_TYP's">?</div></td>
		<td>
			<?php
			$cTypes = array('CMS_HEAD','CMS_HTML','CMS_HTMLHEAD','CMS_IMG','CMS_DATE','CMS_FILELIST',
				'CMS_LINK','CMS_LINKDESCR','CMS_LINKEDITOR','CMS_LINKTARGET','CMS_SWF','CMS_TEASER','CMS_TEXT');
			?>
			<select name="CMS_VAR[2]">
				<?php
				foreach ($cTypes as $value) {
					if("CMS_VALUE[2]" == $value){
						$s="selected=selected";
						$s_choosed = true;
					}else $s='';
					echo '<option '.$s.' value="'.$value.'">'.$value.'</option>';
				}
				if(!$s_choosed) {
					echo '<option selected=selected value="-1">'.mi18n("Please Choose").'</option>';
					$s_choosed = false;
				}
				?>
			</select>
	</tr>
	<!-- CSS-Style for element -->
	<tr>
		<td><?php echo mi18n("CSS-Class");?></td>
		<td><input type="text" name="<?php echo "CMS_VAR[4]";?>" value="<?php echo "CMS_VALUE[4]"; ?>"></td>
	</tr>

	<tr>
		<td><?php echo mi18n("Modul Template");?></td>
		<td>
			<select name="CMS_VAR[3]">
				<?php
				$module = new cModuleHandler($cCurrentModule);
				$files = $module->getAllFilesFromDirectory('template');
				foreach ($files as $value) {
					if("CMS_VALUE[3]" == $value){
						$s="selected=selected";
						$s_choosed =true;
					}else $s='';
					echo '<option '.$s.' value="'.$value.'">'.$value.'</option>';
				}
				if(!$s_choosed) echo '<option selected=selected value="-1">'.mi18n("Please Choose").'</option>';
                ?>
			</select>
	</tr>
</table>
<?php