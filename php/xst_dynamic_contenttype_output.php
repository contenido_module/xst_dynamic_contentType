<?php
/***********************************************
 * Modulname :   xst_dynamic_contentType
 * Author(s) :   Samuel Suther
 * Copyright :   Suther Kommunikationsdesign (www.suther.de)
 ************************************************/

defined('CON_FRAMEWORK') || die('Illegal call: Missing framework initialization - request aborted.');

$container_id= (int) "CMS_VALUE[1]";
$content_type= "CMS_VALUE[2]";
$tpl_file	 = "CMS_VALUE[3]";
$css_class	 = "CMS_VALUE[4]";

$ocType 	= new cTypeGenerator();
$cmsTag 	= new stdClass();
$cmsTag->tag= stripslashes($ocType->getGeneratedCmsTag($content_type, $container_id));
if(!function_exists("getXstImage")){
    function getXstImage($cmsTag){
        global $idartlang,$container_id;

        // build class containing all data necessary to display image
        // therefor the image dimensions have to be determined
        if (0 < strlen($cmsTag->tag)) {
            $art        = new cApiArticleLanguage($idartlang, true);
			$art->loadByArticleAndLanguageId(cRegistry::getArticleId(), cRegistry::getLanguageId());

            $artReturn  = $art->getContent('CMS_IMGEDITOR', $container_id);

            $img      = new cContentTypeImgeditor($artReturn, $container_id, array());
            $filename   = $img->getAbsolutePath();
            $metadata   = (object) $img->getMetaData();

            list($imageWidth, $imageHeight) = getimagesize($filename);
            $image = new stdClass();
            $image->src             = $cmsTag->tag;
            $image->width           = $imageWidth;
            $image->height          = $imageHeight;

            $image->description     = $metadata->description;
            $image->title       	= $metadata->medianame;
            $image->keywords        = $metadata->keywords;
            $image->copyright       = $metadata->copyright;
            $image->internalnotice  = $metadata->internalnotice;
        } else {
            $image = $cmsTag;
        }
        return $image;
    }
}
//Backend-VIEW
if (cRegistry::isBackendEditMode()) {
    $label = NULL;
    switch($content_type){
        case "CMS_IMG":
            $cmsTag = getXstImage($cmsTag);
            $cmsTag->editor	= stripslashes($ocType->getGeneratedCmsTag("CMS_IMGEDITOR", $container_id));
            $cmsTag->desc	= strip_tags(stripslashes($ocType->getGeneratedCmsTag("CMS_IMGDESCR", $container_id)));
            $cmsTag->css	= $css_class;
            break;
        case "CMS_HTML_HEAD":
            $cmsTag->tag	= trim($cmsTag->tag);
            $cmsTag->css	= $css_class;
            break;
        default:
    }
} else {
//Frontend-VIEW
    $label 	= NULL;
    $cmsTag->tag = str_replace(' ', ' ', $cmsTag->tag);
    switch($tpl_file){
        case "h1.html":
        case "h2.html":
        case "h3.html":
            if($content_type=="CMS_HTMLHEAD"){  //allow only specific HTML-Tags
                $cmsTag->tag = strip_tags(trim($cmsTag->tag),"<span><img><a><div>");
            }else {
                $cmsTag->tag = trim($cmsTag->tag);
            }
            $cmsTag->css = $css_class;
            break;
        case "image.html":
            $cmsTag = getXstImage($cmsTag);
            $cmsTag->desc	= strip_tags(stripslashes($ocType->getGeneratedCmsTag("CMS_IMGDESCR", $container_id)));
            $cmsTag->css 	= $css_class;
            break;
        default:
            $cmsTag->tag = trim($cmsTag->tag);
            $cmsTag->css = $css_class;
    }
}
// use smarty template to output the Tag
if( (!empty($cmsTag->tag) || !empty($cmsTag->src) || cRegistry::isBackendEditMode() ) && $container_id){
    $tpl = cSmartyFrontend::getInstance();
	$tpl->assign('debug', false);
    $tpl->assign('label', $label);
    $tpl->assign('cmsTag', $cmsTag);
    $tpl->assign('isBackendEditMode', cRegistry::isBackendEditMode());
    $tpl->display($tpl_file);
}
?>