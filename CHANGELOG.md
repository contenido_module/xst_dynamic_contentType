# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.5.3] - 2016-11-30
### Fixed
 - shorten the Comment in Module-In & Output, move Module-Meta-Infos to infos.xml (Module-Description-Field)
 - make "Please Choose" Option in Module-Input selected, if nothing else is choosed
 - Only output Template, if Container-ID is set, else do nothing [prevent for SmartyException "Missing Template name"
 - TypeCast container_id to (int)


## [0.5.2] - 2016-11-29
### Added
 - Don't display Template, if cmsTag->tag or ->src is empty... but do if in BackendMode
### Fixed
 - Fix missing Image-Metadata [needs a cast from array => Obj.]


## [0.5.1] - 2016-11-25
### Added
 - Description to Module-Description-Area

### Fixed
 - return Object in function getXstImage(), if no Image was set to prevent an PHP-Error: _Creating default object from empty value_
 - change $tbl to $tpl

## [0.5.0] - 2016-10-12
### Changed
 - add Metadata for images in Module
 - add description, which Variables available in tpl: image.html


## [0.4.0] - 2015-04-13
 - add CSS-Class-Field to set a css-class for each Content-Type via module configuration
 - Code optimized ($cmsTag as Object now)
 - removed strip_tags on h1.html-tpl-file
 - add css-class to tpl.-files, if setup in modul configuration


## [0.3.0] - 2013-11-27
### Initialize Project