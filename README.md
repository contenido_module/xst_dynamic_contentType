xst_dynamic_contentType
=======================

Type: 		Contenido Module  
Name: 		xst_dynamic_contentType  
Autor: 		Samuel Suther  
Company: 	Suther Kommunikationsdesign  
Website: 	http://www.suther.de  


[CHANGELOG](https://gitlab.com/contenido_module/xst_dynamic_contentType/blob/master/CHANGELOG.md)